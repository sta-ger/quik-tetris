const path = require("path");

module.exports = {
    entry: "./src/Main.ts",
    devtool: "source-map",
    module: {
        rules: [
            {
                use: "ts-loader",
                exclude: /node_modules/,
            },
        ],
    },
    resolve: {
        extensions: [".ts", ".js"],
    },
    output: {
        filename: "game.js",
        path: path.resolve(__dirname, "build"),
    },
};