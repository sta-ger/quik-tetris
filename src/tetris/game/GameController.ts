import * as PIXI from "pixi.js";

import {Glass} from "./elements/glass/Glass";
import {IGlass} from "./elements/glass/IGlass";
import {App} from "../App";
import {KeyboardEventKey} from "../KeyboardEventKey";
import {KeyboardEventType} from "../KeyboardEventType";
import {GameEvent} from "./GameEvent";

export class GameController {
    private normalUpdateInterval: number;
    private fastUpdateInterval: number;
    private glass: IGlass;
    private app: App;
    private updateTimeoutId: number;
    private currentUpdateInterval: number;
    private score: number = 0;
    private scoreTextField: PIXI.Text;
    private isGameOver: boolean;

    constructor(app: App) {
        this.app = app;
        this.initializeParameters();
        this.initialize();
    }

    protected initializeParameters(): void {
        this.normalUpdateInterval = 700;
        this.fastUpdateInterval = 50;
    }

    protected initialize(): void {
        this.app.on(KeyboardEventType.KEY_UP, (key: string) => this.onKeyUpEvent(key));
        this.app.on(KeyboardEventType.KEY_DOWN, (key: string) => this.onKeyDownEvent(key));
        this.glass = this.createGlass();
        this.glass.on(GameEvent.BLOCKS_EXPLODED, (explodedRowsNum: number) => this.onBlocksExploded(explodedRowsNum));
        this.glass.getDisplay().y = this.app.getGamePaddingTop();
        this.app.getStage().addChild(this.glass.getDisplay());
        this.scoreTextField = this.createScoreTextField();
        this.scoreTextField.x = this.app.getGameWidth() / 2;
        this.app.getStage().addChild(this.scoreTextField);
        this.startGame();
    }

    private onBlocksExploded(explodedRowsNum: number): void {
        this.setScore(this.score + 10 * explodedRowsNum);
    }

    private startGame(): void {
        this.currentUpdateInterval = this.normalUpdateInterval;
        this.glass.setNewFigure();
        this.glass.resetFigurePosition();
        this.update();
    }

    private update(): void {
        this.drawGameStep();
        this.requestUpdate();
    }

    private requestUpdate(): void {
        if (!this.isGameOver) {
            this.updateTimeoutId = window.setTimeout(() => this.update(), this.currentUpdateInterval);
        }
    }

    private drawGameStep(): void {
        this.glass.incrementFigurePositionY();
        if (this.glass.hasFigureAnyCollision()) {
            this.glass.decrementFigurePositionY();
            this.glass.addCurrentFigureToWall();
            this.glass.setNewFigure();
            this.glass.resetFigurePosition();
            if (this.glass.isImpossibleToCreateFigure()) {
                this.setGameOver();
            } else {
                this.drawGameStep();
            }
        } else {
            this.glass.drawFigurePosition();
        }
    }

    private onKeyUpEvent(key: string): void {
        switch (key) {
            case KeyboardEventKey.LEFT:
                this.onLeftKeyUp();
                break;
            case KeyboardEventKey.RIGHT:
                this.onRightKeyUp();
                break;
            case KeyboardEventKey.UP:
                this.onUpKeyUp();
                break;
            case KeyboardEventKey.DOWN:
                this.onDownKeyUp();
                break;
        }
    }

    private onKeyDownEvent(key: string): void {
        switch (key) {
            case KeyboardEventKey.DOWN:
                this.onDownKeyDown();
                break;
        }
    }

    private onLeftKeyUp(): void {
        if (!this.isGameOver) {
            this.glass.decrementFigurePositionX();
            if (this.glass.hasFigureAnyCollision()) {
                this.glass.incrementFigurePositionX();
            }
            this.glass.drawFigurePosition();
        }
    }

    private onRightKeyUp(): void {
        if (!this.isGameOver) {
            this.glass.incrementFigurePositionX();
            if (this.glass.hasFigureAnyCollision()) {
                this.glass.decrementFigurePositionX();
            }
            this.glass.drawFigurePosition();
        }
    }

    private onUpKeyUp(): void {
        if (!this.isGameOver) {
            this.glass.rotateFigure();
            this.glass.drawFigurePosition();
        }
    }

    private onDownKeyUp(): void {
        if (!this.isGameOver) {
            this.currentUpdateInterval = this.normalUpdateInterval;
        }
    }

    private onDownKeyDown(): void {
        if (!this.isGameOver) {
            this.currentUpdateInterval = this.fastUpdateInterval;
            window.clearTimeout(this.updateTimeoutId);
            this.update();
        }
    }

    private setScore(points: number): void {
        this.score = points;
        this.scoreTextField.text = "Score: " + points.toString();
    }

    private setGameOver(): void {
        this.isGameOver = true;
        this.scoreTextField.text = "Game over";
        window.clearTimeout(this.updateTimeoutId);
    }

    protected createGlass(): IGlass {
        return new Glass();
    }

    protected createScoreTextField(): PIXI.Text {
        const s: PIXI.TextStyle = new PIXI.TextStyle();
        s.fill = 0xFFFFFF;
        s.fontSize = 20;
        const tf: PIXI.Text = new PIXI.Text("Score: 0", s);
        tf.anchor.x = 0.5;
        tf.roundPixels = true;
        return tf;
    }

}
