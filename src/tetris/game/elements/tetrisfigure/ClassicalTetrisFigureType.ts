export class ClassicalTetrisFigureType {

	public static readonly I: string = "I";
	public static readonly L: string = "L";
	public static readonly J: string = "J";
	public static readonly S: string = "S";
	public static readonly T: string = "T";
	public static readonly O: string = "O";
	public static readonly Z: string = "Z";

}
