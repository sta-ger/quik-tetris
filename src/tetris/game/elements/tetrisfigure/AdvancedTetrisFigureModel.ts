import {BlockColor} from "../block/BlockColor";
import {AdvancedTetrisFigureType} from "./AdvancedTetrisFigureType";
import {TetrisFigureState} from "./TetrisFigureState";
import {ClassicalTetrisFigureModel} from "./ClassicalTetrisFigureModel";

export class AdvancedTetrisFigureModel extends ClassicalTetrisFigureModel {

    constructor(type: string, state: string) {
        super(type, state);
    }

    protected createFigureColor(): string {
        let returnVal: string;
        switch (this.type) {
            case AdvancedTetrisFigureType.FIGURE_1:
                returnVal = BlockColor.YELLOW;
                break;
            case AdvancedTetrisFigureType.FIGURE_2:
                returnVal = BlockColor.VIOLET;
                break;
            case AdvancedTetrisFigureType.FIGURE_3:
                returnVal = BlockColor.PINK;
                break;
            case AdvancedTetrisFigureType.FIGURE_4:
                returnVal = BlockColor.GREEN;
                break;
            case AdvancedTetrisFigureType.FIGURE_5:
                returnVal = BlockColor.BLUE;
                break;
        }
        return returnVal;
    }

    protected createFigureMatrix(): number[][] {
        let returnVal: number[][];
        switch (this.type) {
            case AdvancedTetrisFigureType.FIGURE_1:
                switch (this.state) {
                    case TetrisFigureState.STATE_1:
                        returnVal = [
                            [1, 0],
                            [1, 1],
                        ];
                        break;
                    case TetrisFigureState.STATE_2:
                        returnVal = [
                            [1, 1],
                            [1, 0],
                        ];
                        break;
                    case TetrisFigureState.STATE_3:
                        returnVal = [
                            [1, 1],
                            [0, 1],
                        ];
                        break;
                    case TetrisFigureState.STATE_4:
                        returnVal = [
                            [0, 1],
                            [1, 1],
                        ];
                        break;
                }
                break;
            case AdvancedTetrisFigureType.FIGURE_2:
                switch (this.state) {
                    case TetrisFigureState.STATE_1:
                        returnVal = [
                            [1, 1, 1]
                        ];
                        break;
                    case TetrisFigureState.STATE_2:
                        returnVal = [
                            [1],
                            [1],
                            [1],
                        ];
                        break;
                }
                break;
            case AdvancedTetrisFigureType.FIGURE_3:
                switch (this.state) {
                    case TetrisFigureState.STATE_1:
                        returnVal = [
                            [1, 0, 1],
                            [0, 1, 0]
                        ];
                        break;
                    case TetrisFigureState.STATE_2:
                        returnVal = [
                            [0, 1],
                            [1, 0],
                            [0, 1]
                        ];
                        break;
                    case TetrisFigureState.STATE_3:
                        returnVal = [
                            [0, 1, 0],
                            [1, 0, 1]
                        ];
                        break;
                    case TetrisFigureState.STATE_4:
                        returnVal = [
                            [1, 0],
                            [0, 1],
                            [1, 0]
                        ];
                        break;
                }
                break;
            case AdvancedTetrisFigureType.FIGURE_4:
                switch (this.state) {
                    case TetrisFigureState.STATE_1:
                        returnVal = [
                            [1, 0, 0],
                            [0, 1, 1]
                        ];
                        break;
                    case TetrisFigureState.STATE_2:
                        returnVal = [
                            [0, 1],
                            [1, 0],
                            [1, 0]
                        ];
                        break;
                    case TetrisFigureState.STATE_3:
                        returnVal = [
                            [1, 1, 0],
                            [0, 0, 1]
                        ];
                        break;
                    case TetrisFigureState.STATE_4:
                        returnVal = [
                            [0, 1],
                            [0, 1],
                            [1, 0]
                        ];
                        break;
                }
                break;
            case AdvancedTetrisFigureType.FIGURE_5:
                switch (this.state) {
                    case TetrisFigureState.STATE_1:
                        returnVal = [
                            [1, 0, 0],
                            [0, 1, 0],
                            [0, 0, 1],
                        ];
                        break;
                    case TetrisFigureState.STATE_2:
                        returnVal = [
                            [0, 0, 1],
                            [0, 1, 0],
                            [1, 0, 0],
                        ];
                        break;
                }
                break;
        }
        return returnVal;
    }

}

