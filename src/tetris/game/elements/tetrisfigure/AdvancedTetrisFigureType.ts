export class AdvancedTetrisFigureType {

    public static readonly FIGURE_1: string = "figure1";
    public static readonly FIGURE_2: string = "figure2";
    public static readonly FIGURE_3: string = "figure3";
    public static readonly FIGURE_4: string = "figure4";
    public static readonly FIGURE_5: string = "figure5";

}