import {BlockColor} from "../block/BlockColor";
import {BlockModel} from "../block/BlockModel";
import {FigureModel} from "../figure/FigureModel";
import {ClassicalTetrisFigureType} from "./ClassicalTetrisFigureType";
import {TetrisFigureState} from "./TetrisFigureState";

export class ClassicalTetrisFigureModel extends FigureModel {
	protected readonly type: string;
	protected readonly state: string;
	protected color: string;

	constructor(type: string, state: string) {
		super();
		this.type = type;
		this.state = state;
		this.initialize();
	}

	protected initialize(): void {
		if (this.type && this.state) {
			this.color = this.createFigureColor();
			super.initialize();
		}
	}

	protected createFigureColor(): string {
		let returnVal: string;
		switch (this.type) {
			case ClassicalTetrisFigureType.I:
				returnVal = BlockColor.YELLOW;
				break;
			case ClassicalTetrisFigureType.J:
				returnVal = BlockColor.VIOLET;
				break;
			case ClassicalTetrisFigureType.L:
				returnVal = BlockColor.PINK;
				break;
			case ClassicalTetrisFigureType.O:
				returnVal = BlockColor.GREEN;
				break;
			case ClassicalTetrisFigureType.S:
				returnVal = BlockColor.BLUE;
				break;
			case ClassicalTetrisFigureType.T:
				returnVal = BlockColor.YELLOW;
				break;
			case ClassicalTetrisFigureType.Z:
				returnVal = BlockColor.VIOLET;
				break;
		}
		return returnVal;
	}

	protected createFigureMatrix(): number[][] {
		let returnVal: number[][];
		switch (this.type) {
			case ClassicalTetrisFigureType.I:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[1],
							[1],
							[1],
							[1]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[1, 1, 1, 1]
						];
						break;
				}
				break;
			case ClassicalTetrisFigureType.J:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[1, 0, 0],
							[1, 1, 1]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[1, 1],
							[1, 0],
							[1, 0]
						];
						break;
					case TetrisFigureState.STATE_3:
						returnVal = [
							[1, 1, 1],
							[0, 0, 1]
						];
						break;
					case TetrisFigureState.STATE_4:
						returnVal = [
							[0, 1],
							[0, 1],
							[1, 1]
						];
						break;
				}
				break;
			case ClassicalTetrisFigureType.L:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[0, 0, 1],
							[1, 1, 1]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[1, 0],
							[1, 0],
							[1, 1]
						];
						break;
					case TetrisFigureState.STATE_3:
						returnVal = [
							[1, 1, 1],
							[1, 0, 0]
						];
						break;
					case TetrisFigureState.STATE_4:
						returnVal = [
							[1, 1],
							[0, 1],
							[0, 1]
						];
						break;
				}
				break;
			case ClassicalTetrisFigureType.O:
				returnVal = [
					[1, 1],
					[1, 1]
				];
				break;
			case ClassicalTetrisFigureType.S:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[0, 1, 1],
							[1, 1, 0]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[1, 0],
							[1, 1],
							[0, 1]
						];
						break;
				}
				break;
			case ClassicalTetrisFigureType.T:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[0, 1, 0],
							[1, 1, 1]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[1, 0],
							[1, 1],
							[1, 0]
						];
						break;
					case TetrisFigureState.STATE_3:
						returnVal = [
							[1, 1, 1],
							[0, 1, 0]
						];
						break;
					case TetrisFigureState.STATE_4:
						returnVal = [
							[0, 1],
							[1, 1],
							[0, 1]
						];
						break;
				}
				break;
			case ClassicalTetrisFigureType.Z:
				switch (this.state) {
					case TetrisFigureState.STATE_1:
						returnVal = [
							[1, 1, 0],
							[0, 1, 1]
						];
						break;
					case TetrisFigureState.STATE_2:
						returnVal = [
							[0, 1],
							[1, 1],
							[1, 0]
						];
						break;
				}
				break;
		}
		return returnVal;
	}

	protected createBlockModel(x: number, y: number): BlockModel {
		return new BlockModel(x, y, this.color);
	}

}

