import {FigureModel} from "../figure/FigureModel";
import {BlockModel} from "../block/BlockModel";

export class JsonTetrisFigureModel extends FigureModel {
    private readonly type: string;
    private readonly state: string;
    private readonly matrixFromJson: number[][];
    private readonly color: string;

    constructor(type: string, state: string, matrix: number[][], colorFromJson: string) {
        super();
        this.type = type;
        this.state = state;
        this.matrixFromJson = matrix;
        this.color = colorFromJson;
        this.initialize();
    }

    protected createFigureMatrix(): number[][] {
        if (this.matrixFromJson) {
            return this.matrixFromJson;
        } else {
            return super.createFigureMatrix();
        }
    }

    protected createBlockModel(x: number, y: number): BlockModel {
        return super.createBlockModel(x, y, this.color);
    }
}

