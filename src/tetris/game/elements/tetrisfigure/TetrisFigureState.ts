export class TetrisFigureState {

    public static readonly STATE_1: string = "1";
    public static readonly STATE_2: string = "2";
    public static readonly STATE_3: string = "3";
    public static readonly STATE_4: string = "4";

}
