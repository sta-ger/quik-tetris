import * as PIXI from "pixi.js";

export interface IDisplayingObject {

    getDisplay(): PIXI.DisplayObject;

}