import * as PIXI from "pixi.js";
import {IFigure} from "../figure/IFigure";
import {IFallingFigure} from "./IFallingFigure";
import {IFallingFigureFiguresFactory} from "./IFallingFigureFiguresFactory";
import {FallingFigureJsonTetrisFiguresFactory} from "./FallingFigureJsonTetrisFiguresFactory";

export class FallingFigure implements IFallingFigure {
	protected type: string;
	protected state: string;

	private figuresContainer: PIXI.Container;
	private figuresByStates: { [state: string]: IFigure };
	private currentStateIndex: number = 0;
	private currentFigure: IFigure;
	private figuresFactory: IFallingFigureFiguresFactory;

	constructor(type?: string, state?: string) {
		this.type = type;
		this.state = state;
		this.initialize();
	}

	protected initialize(): void {
		this.figuresContainer = this.createFiguresContainer();
		this.figuresFactory = this.createFiguresFactory();

		if (this.type === undefined) {
			this.type = this.getRandomFigureType();
		}

		if (this.state === undefined) {
			this.state = this.getRandomFigureState();
		}

		this.figuresByStates = this.createFiguresByStates();

		this.setState(this.state);
	}

	protected createFiguresByStates(): { [state: string]: IFigure } {
		const returnVal: {} = {};
		this.figuresFactory.getAvailableFigureStates(this.type).forEach((state: string) => {
			returnVal[state] = this.figuresFactory.createFigure(this.type, state);
		});
		return returnVal;
	}

	public setPreviousState(): void {
		this.currentStateIndex--;
		if (this.currentStateIndex < 0) {
			this.currentStateIndex = this.figuresFactory.getAvailableFigureStates(this.type).length - 1;
		}
		this.setState(this.figuresFactory.getAvailableFigureStates(this.type)[this.currentStateIndex]);
	}

	public setNextState(): void {
		this.currentStateIndex++;
		if (this.currentStateIndex > this.figuresFactory.getAvailableFigureStates(this.type).length - 1) {
			this.currentStateIndex = 0;
		}
		this.setState(this.figuresFactory.getAvailableFigureStates(this.type)[this.currentStateIndex]);
	}

	private setState(state: string): void {
		const index: number = this.figuresFactory.getAvailableFigureStates(this.type).indexOf(state);
		if (index >= 0) {
			this.currentStateIndex = index;
			this.currentFigure = this.figuresByStates[state];
			this.hideFigures();
			this.figuresContainer.addChild(this.currentFigure.getDisplay());
		} else {
			throw "Wrong state";
		}
	}

	private hideFigures(): void {
		while (this.figuresContainer.children.length > 0) {
			this.figuresContainer.removeChildAt(0);
		}
	}

	public getCurrentFigure(): IFigure {
		return this.currentFigure;
	}

	protected createFiguresFactory(): IFallingFigureFiguresFactory {
		// return new FallingFigureClassicalTetrisFiguresFactory();
		// return new FallingFigureAdvancedTetrisFiguresFactory();
		return new FallingFigureJsonTetrisFiguresFactory();
	}

	protected createFiguresContainer(): PIXI.Container {
		return new PIXI.Container();
	}

	private getRandomFigureType(): string {
		const allTypes: string[] = this.figuresFactory.getAvailableFigureTypes();
		return allTypes[Math.floor(Math.random() * allTypes.length)];
	}

	private getRandomFigureState(): string {
		const allStates: string[] = this.figuresFactory.getAvailableFigureStates(this.type);
		return allStates[Math.floor(Math.random() * allStates.length)];
	}

	public getDisplay(): PIXI.DisplayObject {
		return this.figuresContainer;
	}

}

