import {IFigure} from "../figure/IFigure";

export interface IFallingFigureFiguresFactory {

    getAvailableFigureTypes(): string[];

    getAvailableFigureStates(figureType: string): string[];

    createFigure(type: string, state: string): IFigure;

}