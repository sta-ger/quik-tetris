import {IFallingFigureFiguresFactory} from "./IFallingFigureFiguresFactory";
import {IFigure} from "../figure/IFigure";
import {Figure} from "../figure/Figure";
import {JsonTetrisFigureModel} from "../tetrisfigure/JsonTetrisFigureModel";
import {App} from "../../../App";
import {Resource} from "../../../resources/Resource";
import {FiguresJsonData, SingleFigureJsonData} from "../../../FiguresDataJsonType";

export class FallingFigureJsonTetrisFiguresFactory implements IFallingFigureFiguresFactory {
    private readonly jsonData: FiguresJsonData;

    constructor() {
        this.jsonData = App.resourcesManager.getJson(Resource.JSON_FIGURES) as FiguresJsonData;
    }

    public createFigure(type: string, state: string): IFigure {
        const figureJsonData: SingleFigureJsonData = this.jsonData.availableFigures[type];
        const matrix: number[][] = figureJsonData.states[state];
        const color: string = figureJsonData.color;
        return new Figure(
            new JsonTetrisFigureModel(type, state, matrix, color)
        );
    }

    public getAvailableFigureTypes(): string[] {
        return Object.keys(this.jsonData.availableFigures);
    }

    public getAvailableFigureStates(figureType: string): string[] {
        return Object.keys(this.jsonData.availableFigures[figureType].states);
    }

}