import {IFigure} from "../figure/IFigure";
import {IDisplayingObject} from "../IDisplayingObject";

export interface IFallingFigure extends IDisplayingObject {

    setPreviousState(): void;

    setNextState(): void;

    getCurrentFigure(): IFigure;

}