import {IFallingFigureFiguresFactory} from "./IFallingFigureFiguresFactory";
import {ClassicalTetrisFigureType} from "../tetrisfigure/ClassicalTetrisFigureType";
import {TetrisFigureState} from "../tetrisfigure/TetrisFigureState";
import {ClassicalTetrisFigureModel} from "../tetrisfigure/ClassicalTetrisFigureModel";
import {IFigure} from "../figure/IFigure";
import {Figure} from "../figure/Figure";

export class FallingFigureClassicalTetrisFiguresFactory implements IFallingFigureFiguresFactory {

    public createFigure(type: string, state: string): IFigure {
        return new Figure(
            new ClassicalTetrisFigureModel(type, state)
        );
    }

    public getAvailableFigureTypes(): string[] {
        return [
            ClassicalTetrisFigureType.I,
            ClassicalTetrisFigureType.L,
            ClassicalTetrisFigureType.J,
            ClassicalTetrisFigureType.S,
            ClassicalTetrisFigureType.T,
            ClassicalTetrisFigureType.O,
            ClassicalTetrisFigureType.Z,
        ];
    }

    public getAvailableFigureStates(figureType: string): string[] {
        let returnVal: string[];
        switch (figureType) {
            case ClassicalTetrisFigureType.I:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2
                ];
                break;
            case ClassicalTetrisFigureType.J:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4
                ];
                break;
            case ClassicalTetrisFigureType.L:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4
                ];
                break;
            case ClassicalTetrisFigureType.S:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2
                ];
                break;
            case ClassicalTetrisFigureType.T:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4
                ];
                break;
            case ClassicalTetrisFigureType.Z:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2
                ];
                break;
            case ClassicalTetrisFigureType.O:
                returnVal = [
                    TetrisFigureState.STATE_1
                ];
                break;
        }
        return returnVal;
    }

}