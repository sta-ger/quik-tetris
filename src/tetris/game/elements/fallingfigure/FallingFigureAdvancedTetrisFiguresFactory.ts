import {IFallingFigureFiguresFactory} from "./IFallingFigureFiguresFactory";
import {AdvancedTetrisFigureType} from "../tetrisfigure/AdvancedTetrisFigureType";
import {TetrisFigureState} from "../tetrisfigure/TetrisFigureState";
import {AdvancedTetrisFigureModel} from "../tetrisfigure/AdvancedTetrisFigureModel";
import {IFigure} from "../figure/IFigure";
import {Figure} from "../figure/Figure";

export class FallingFigureAdvancedTetrisFiguresFactory implements IFallingFigureFiguresFactory {

    public createFigure(type: string, state: string): IFigure {
        return new Figure(
            new AdvancedTetrisFigureModel(type, state)
        );
    }

    public getAvailableFigureTypes(): string[] {
        return [
            AdvancedTetrisFigureType.FIGURE_1,
            AdvancedTetrisFigureType.FIGURE_2,
            AdvancedTetrisFigureType.FIGURE_3,
            AdvancedTetrisFigureType.FIGURE_4,
            AdvancedTetrisFigureType.FIGURE_5,
        ];
    }

    public getAvailableFigureStates(figureType: string): string[] {
        let returnVal: string[];
        switch (figureType) {
            case AdvancedTetrisFigureType.FIGURE_1:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4,
                ];
                break;
            case AdvancedTetrisFigureType.FIGURE_2:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                ];
                break;
            case AdvancedTetrisFigureType.FIGURE_3:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4,
                ];
                break;
            case AdvancedTetrisFigureType.FIGURE_4:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                    TetrisFigureState.STATE_3,
                    TetrisFigureState.STATE_4,
                ];
                break;
            case AdvancedTetrisFigureType.FIGURE_5:
                returnVal = [
                    TetrisFigureState.STATE_1,
                    TetrisFigureState.STATE_2,
                ];
                break;
        }
        return returnVal;
    }

}