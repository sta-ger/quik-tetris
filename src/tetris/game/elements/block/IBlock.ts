import {IDisplayingObject} from "../IDisplayingObject";

export interface IBlock extends IDisplayingObject {}