export class BlockColor {

	public static readonly GREY: string = "grey";
	public static readonly YELLOW: string = "yellow";
	public static readonly VIOLET: string = "violet";
	public static readonly PINK: string = "pink";
	public static readonly GREEN: string = "green";
	public static readonly BLUE: string = "blue";

}
