import {App} from "../../../App";
import {Resource} from "../../../resources/Resource";
import {BlockColor} from "./BlockColor";
import {BlockModel} from "./BlockModel";
import {IBlock} from "./IBlock";

export class Block implements IBlock {
	private readonly model: BlockModel;
	private asset: PIXI.DisplayObject;
	private blockWidth: number;
	private blockHeight: number;

	constructor(model: BlockModel) {
		this.model = model;
		this.initializeParameters();
		this.initialize();
	}

	protected initializeParameters(): void {
		this.blockWidth = App.BLOCK_WIDTH;
		this.blockHeight = App.BLOCK_HEIGHT;
	}

	protected initialize(): void {
		this.asset = this.createAsset();
		this.setX(this.model.getOrderX() * this.blockWidth);
		this.setY(this.model.getOrderY() * this.blockHeight);
	}

	protected createAsset(): PIXI.DisplayObject {
		let returnVal: PIXI.Sprite;
		switch (this.model.getColor()) {
			case BlockColor.GREY:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_GREY);
				break;
			case BlockColor.YELLOW:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_YELLOW);
				break;
			case BlockColor.VIOLET:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_VIOLET);
				break;
			case BlockColor.PINK:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_PINK);
				break;
			case BlockColor.GREEN:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_GREEN);
				break;
			case BlockColor.BLUE:
				returnVal = App.resourcesManager.createSprite(Resource.TEXTURE_BLOCK_BLUE);
				break;
		}
		returnVal.width = this.blockWidth;
		returnVal.height = this.blockHeight;
		return returnVal;
	}

	private setX(value: number): void {
		this.getDisplay().x = value;
	}

	private setY(value: number): void {
		this.getDisplay().y = value;
	}

	public getDisplay(): PIXI.DisplayObject {
		return this.asset;
	}

}
