export class BlockModel {
	private readonly xOrder: number;
	private readonly yOrder: number;
	private readonly color: string;

	constructor(xOrder: number, yOrder: number, color: string) {
		this.xOrder = xOrder;
		this.yOrder = yOrder;
		this.color = color;
	}

	public getColor(): string {
		return this.color;
	}

	public getOrderX(): number {
		return this.xOrder;
	}

	public getOrderY(): number {
		return this.yOrder;
	}

}
