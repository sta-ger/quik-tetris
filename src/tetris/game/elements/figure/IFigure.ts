import {IDisplayingObject} from "../IDisplayingObject";
import {FigureModel} from "./FigureModel";

export interface IFigure extends IDisplayingObject {

    buildFigure(): void;

    getTopmostBlockPosition(): { [xy: string]: number };

    getModel(): FigureModel;

}