import {BlockColor} from "../block/BlockColor";
import {BlockModel} from "../block/BlockModel";

export class FigureModel {
	private matrix: number[][];
	private blocksMap: { [xColonY: string]: BlockModel };

	constructor() {
		this.initialize();
	}

	protected initialize(): void {
		this.matrix = this.createFigureMatrix();
		this.fillBlocksModelsMap();
	}

	protected createFigureMatrix(): number[][] {
		return [
			[1]
		];
	}

	public fillBlocksModelsMap(): void {
		this.blocksMap = {};
		const matrixW: number = this.getMatrixWidth();
		const matrixH: number = this.getMatrixHeight();
		const matrix: number[][] = this.getMatrix();
		for (let i: number = 0; i < matrixH; i++) {
			for (let j: number = 0; j < matrixW; j++) {
				if (matrix[i][j] !== 0) {
					this.addBlockModelToMap(this.createBlockModel(j, i, BlockColor.GREY));
				}
			}
		}
	}

	public getBlocksMap(): { [xColonY: string]: BlockModel } {
		return this.blocksMap;
	}

	public getMatrixWidth(): number {
		return this.matrix[0].length;
	}

	public getMatrixHeight(): number {
		return this.matrix.length;
	}

	private addBlockModelToMap(blockModel: BlockModel): void {
		this.blocksMap[blockModel.getOrderX().toString() + ":" + blockModel.getOrderY().toString()] = blockModel;
	}

	public getBlockModelAt(x: number, y: number): BlockModel {
		return this.blocksMap[x.toString() + ":" + y.toString()];
	}

	public getMatrix(): number[][] {
		return this.matrix;
	}

	public setMatrix(value: number[][]): void {
		this.matrix = value;
	}

	protected createBlockModel(x: number, y: number, color: string): BlockModel {
		return new BlockModel(x, y, color);
	}

}
