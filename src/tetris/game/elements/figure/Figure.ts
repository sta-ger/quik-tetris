import * as PIXI from "pixi.js";
import {Block} from "../block/Block";
import {FigureModel} from "./FigureModel";
import {App} from "../../../App";
import {IBlock} from "../block/IBlock";
import {BlockModel} from "../block/BlockModel";
import {IFigure} from "./IFigure";

export class Figure implements IFigure {
	protected readonly model: FigureModel;

	private blockWidth: number;
	private blockHeight: number;
	private container: PIXI.Container;

	constructor(model: FigureModel) {
		this.model = model;
		this.initializeParameters();
		this.initialize();
	}

	protected initializeParameters(): void {
		this.blockWidth = App.BLOCK_WIDTH;
		this.blockHeight = App.BLOCK_HEIGHT;
	}

	protected initialize(): void {
		this.container = this.createContainer();
		this.buildFigure();
	}

	public buildFigure(): void {
		this.clear();

		const blocksModelsMap: { [xColonY: string]: BlockModel } = this.model.getBlocksMap();
		Object.values(blocksModelsMap).forEach((blockModel: BlockModel) => {
			const block: IBlock = this.createBlock(blockModel);
			this.container.addChild(block.getDisplay());
		});
	}

	private clear(): void {
		while (this.container.children.length > 0) {
			this.container.removeChildAt(0);
		}
	}

	public getTopmostBlockPosition(): { [xy: string]: number } {
		let x: number;
		let y: number;
		const matrixW: number = this.model.getMatrixWidth();
		const matrixH: number = this.model.getMatrixHeight();
		const matrix: number[][] = this.model.getMatrix();
		for (let i: number = 0; i < matrixH; i++) {
			for (let j: number = 0; j < matrixW; j++) {
				if (matrix[i][j] !== 0) {
					x = j;
					y = i;
					break;
				}
			}
			if (x !== undefined) {
				break;
			}
		}
		if (x === undefined) {
			x = matrixW;
		}
		if (y === undefined) {
			y = matrixH;
		}
		return {x, y};
	}

	public getModel(): FigureModel {
		return this.model;
	}

	protected createBlock(model: BlockModel): IBlock {
		return new Block(model);
	}

	protected createContainer(): PIXI.Container {
		return new PIXI.Container();
	}

	public getDisplay(): PIXI.Container {
		return this.container;
	}

}
