import {IFallingFigure} from "../fallingfigure/IFallingFigure";
import {FallingFigure} from "../fallingfigure/FallingFigure";
import {IGlassFallingFiguresFactory} from "./IGlassFallingFiguresFactory";
import {App} from "../../../App";
import {Resource} from "../../../resources/Resource";
import {FiguresJsonData, SingleFallingFigureJsonData} from "../../../FiguresDataJsonType";

export class GlassJsonFallingFiguresFactory implements IGlassFallingFiguresFactory {
    private readonly fallingFigures: SingleFallingFigureJsonData[];

    constructor() {
        const jsonData: FiguresJsonData = App.resourcesManager.getJson(Resource.JSON_FIGURES) as FiguresJsonData;
        this.fallingFigures = jsonData.fallingFigures;
    }

    public createFallingFigure(): IFallingFigure {
        if (this.fallingFigures.length > 0) {
            const data: SingleFallingFigureJsonData = this.fallingFigures.shift();
            const type: string = data.figure;
            const state: string = data.initialState;
            return new FallingFigure(type, state);
        } else {
            return null;
        }
    }

}