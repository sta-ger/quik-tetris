import * as PIXI from "pixi.js";
import EventEmitter = PIXI.utils.EventEmitter;

export interface IGlass extends EventEmitter {

    addCurrentFigureToWall(): void;

    setNewFigure(): void;

    resetFigurePosition(): void;

    clearAllBlocks(): void;

    rotateFigure(): void;

    incrementFigurePositionY(): void;

    decrementFigurePositionY(): void;

    incrementFigurePositionX(): void;

    decrementFigurePositionX(): void;

    hasFigureAnyCollision(): boolean;

    isImpossibleToCreateFigure(): boolean;

    drawFigurePosition(): void;

    getDisplay(): PIXI.DisplayObject;

}