import {IFallingFigure} from "../fallingfigure/IFallingFigure";
import {FallingFigure} from "../fallingfigure/FallingFigure";
import {IGlassFallingFiguresFactory} from "./IGlassFallingFiguresFactory";

export class GlassFallingFiguresFactory implements IGlassFallingFiguresFactory {

    public createFallingFigure(): IFallingFigure {
        return new FallingFigure();
    }

}