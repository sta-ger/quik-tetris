import * as PIXI from "pixi.js";

import {Wall} from "../wall/Wall";
import {WallModel} from "../wall/WallModel";
import {IWall} from "../wall/IWall";
import {IFallingFigure} from "../fallingfigure/IFallingFigure";
import {IGlass} from "./IGlass";
import {App} from "../../../App";
import {GameEvent} from "../../GameEvent";
import {IGlassFallingFiguresFactory} from "./IGlassFallingFiguresFactory";
import {GlassJsonFallingFiguresFactory} from "./GlassJsonFallingFiguresFactory";
import EventEmitter = PIXI.utils.EventEmitter;

export class Glass extends EventEmitter implements IGlass {
    private widthBlocksNumber: number;
    private heightBlocksNumber: number;
    private blockWidth: number;
    private blockHeight: number;
    private container: PIXI.Container;
    private fallingFigureContainer: PIXI.Container;
    private wall: IWall;
    private wallContainer: PIXI.Container;
    private fallingFigure: IFallingFigure;
    private currentFigurePositionX: number;
    private currentFigurePositionY: number;
    private fallingFiguresFactory: IGlassFallingFiguresFactory;

    constructor() {
        super();
        this.initializeParameters();
        this.initialize();
    }

    protected initializeParameters(): void {
        this.widthBlocksNumber = App.WIDTH_BLOCKS_NUMBER;
        this.heightBlocksNumber = App.HEIGHT_BLOCKS_NUMBER;
        this.blockWidth = App.BLOCK_WIDTH;
        this.blockHeight = App.BLOCK_HEIGHT;
    }

    protected initialize(): void {
        this.container = new PIXI.Container();
        this.fallingFigureContainer = new PIXI.Container();
        this.container.addChild(this.fallingFigureContainer);
        this.wall = this.createWall();
        this.wallContainer = new PIXI.Container();
        this.wallContainer.addChild(this.wall.getDisplay());
        this.wall.on(GameEvent.BLOCKS_EXPLODED, (explodedRowsNum: number) => {
            this.onWallBlocksExploded(explodedRowsNum);
        });
        this.container.addChild(this.wallContainer);
        this.fallingFiguresFactory = this.createFallingFiguresFactory();
    }

    private onWallBlocksExploded(explodedRowsNum: number): void {
        this.emit(GameEvent.BLOCKS_EXPLODED, explodedRowsNum);
    }

    public addCurrentFigureToWall(): void {
        this.wall.addFigureAt(
            this.fallingFigure.getCurrentFigure(), this.currentFigurePositionX, this.currentFigurePositionY
        );
    }

    public setNewFigure(): void {
        while (this.fallingFigureContainer.children.length > 0) {
            this.fallingFigureContainer.removeChildAt(0);
        }

        this.fallingFigure = this.createFallingFigure();
        if (this.fallingFigure) {
            this.fallingFigureContainer.addChild(this.fallingFigure.getDisplay());
        }
    }

    public resetFigurePosition(): void {
        if (this.fallingFigure) {
            const widthBlocksNumber: number = this.widthBlocksNumber;
            const figureWidthBlocks: number = this.fallingFigure.getCurrentFigure().getModel().getMatrixWidth();
            this.currentFigurePositionX = Math.floor(widthBlocksNumber / 2) - Math.floor(figureWidthBlocks / 2);
            this.currentFigurePositionY = -1;
        }
    }

    public clearAllBlocks(): void {
        this.wall.clearAllBlocks();
    }

    public rotateFigure(): void {
        this.fallingFigure.setNextState();
        if (this.hasFigureAnyCollision()) {
            this.fallingFigure.setPreviousState();
        }
    }

    public incrementFigurePositionY(): void {
        this.currentFigurePositionY++;
    }

    public decrementFigurePositionY(): void {
        this.currentFigurePositionY--;
    }

    public incrementFigurePositionX(): void {
        this.currentFigurePositionX++;
    }

    public decrementFigurePositionX(): void {
        this.currentFigurePositionX--;
    }

    public hasFigureAnyCollision(): boolean {
        return this.hasFigureAnyColisionBottom() ||
            this.hasFigureAnyCollisionLeft() ||
            this.hasFigureAnyCollisionRight() ||
            this.hasFigureWallCollision();
    }

    private hasFigureWallCollision(): boolean {
        return this.wall.isFigureCollidesWall(
            this.fallingFigure.getCurrentFigure(), this.currentFigurePositionX, this.currentFigurePositionY
        );
    }

    public isImpossibleToCreateFigure(): boolean {
        const wallTopmostBlockY: number = this.wall.getTopmostBlockPosition().y;
        const figureHeight: number = this.fallingFigure?.getCurrentFigure().getModel().getMatrixHeight();
        return !this.fallingFigure || wallTopmostBlockY < figureHeight;
    }

    private hasFigureAnyCollisionLeft(): boolean {
        let returnVal: boolean = false;
        if (this.hasFigureGlassCollisionLeft()) {
            returnVal = true;
        }
        return returnVal;
    }

    private hasFigureAnyCollisionRight(): boolean {
        let returnVal: boolean = false;
        if (this.hasFigureGlassCollisionRight()) {
            returnVal = true;
        }
        return returnVal;
    }

    private hasFigureAnyColisionBottom(): boolean {
        let returnVal: boolean = false;
        if (this.hasFigureGlassCollisionBottom()) {
            returnVal = true;
        }
        return returnVal;
    }

    private hasFigureGlassCollisionLeft(): boolean {
        let returnVal: boolean = false;
        if (this.currentFigurePositionX < 0) {
            returnVal = true;
        }
        return returnVal;
    }

    private hasFigureGlassCollisionRight(): boolean {
        let returnVal: boolean = false;
        const figureWidthBlocks: number = this.fallingFigure.getCurrentFigure().getModel().getMatrixWidth();
        if (this.currentFigurePositionX + figureWidthBlocks > this.widthBlocksNumber) {
            returnVal = true;
        }
        return returnVal;
    }

    private hasFigureGlassCollisionBottom(): boolean {
        let returnVal: boolean = false;
        const figureHeightBlocks: number = this.fallingFigure.getCurrentFigure().getModel().getMatrixHeight();
        if (this.currentFigurePositionY + figureHeightBlocks > this.heightBlocksNumber) {
            returnVal = true;
        }
        return returnVal;
    }

    public drawFigurePosition(): void {
        this.fallingFigure.getDisplay().x = this.currentFigurePositionX * this.blockWidth;
        this.fallingFigure.getDisplay().y = this.currentFigurePositionY * this.blockHeight;
    }

    protected createFallingFiguresFactory(): IGlassFallingFiguresFactory {
        // return new GlassFallingFiguresFactory();
        return new GlassJsonFallingFiguresFactory();
    }

    protected createFallingFigure(): IFallingFigure {
        return this.fallingFiguresFactory.createFallingFigure();
    }

    protected createWall(): IWall {
        return new Wall(new WallModel(this.widthBlocksNumber, this.heightBlocksNumber));
    }

    public getDisplay(): PIXI.DisplayObject {
        return this.container;
    }

}
