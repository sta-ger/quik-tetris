import {IFallingFigure} from "../fallingfigure/IFallingFigure";

export interface IGlassFallingFiguresFactory {

    createFallingFigure(): IFallingFigure;

}

