import {FigureModel} from "../figure/FigureModel";

export class WallModel extends FigureModel {
	private readonly widthBlocksNumber: number;
	private readonly heightBlocksNumber: number;

	constructor(widthBlocksNumber: number, heightBlocksNumber: number) {
		super();
		this.widthBlocksNumber = widthBlocksNumber;
		this.heightBlocksNumber = heightBlocksNumber;
		this.initialize();
	}

	protected initialize(): void {
		if (this.widthBlocksNumber && this.heightBlocksNumber) {
			super.initialize();
		}
	}

	public clear(): void {
		this.setMatrix(this.getMatrix().map((row: number[]) => row.map(() => 0)));
	}

	protected createFigureMatrix(): number[][] {
		const returnVal: number[][] = [];
		for (let i: number = 0; i < this.heightBlocksNumber; i++) {
			returnVal[i] = [];
			for (let j: number = 0; j < this.widthBlocksNumber; j++) {
				returnVal[i][j] = 0;
			}
		}
		return returnVal;
	}

}
