import {IFigure} from "../figure/IFigure";
import EventEmitter = PIXI.utils.EventEmitter;

export interface IWall extends IFigure, EventEmitter {

    addFigureAt(figure, x, y): void;

    isFigureCollidesWall(figure, x, y): boolean;

    clearAllBlocks(): void;

}