import * as PIXI from "pixi.js";
import {Figure} from "../figure/Figure";
import {IWall} from "./IWall";
import {IFigure} from "../figure/IFigure";
import {FigureModel} from "../figure/FigureModel";
import {BlockModel} from "../block/BlockModel";
import {WallModel} from "./WallModel";
import {GameEvent} from "../../GameEvent";
import EventEmitter = PIXI.utils.EventEmitter;

export class Wall extends Figure implements IWall {
    private eventEmitter: PIXI.utils.EventEmitter;

    constructor(model: FigureModel) {
        super(model);
        this.eventEmitter = new EventEmitter();
    }

    public addFigureAt(figure: IFigure, x: number, y: number): void {
        const figureModel: FigureModel = figure.getModel();
        const figureMatrix: number[][] = figureModel.getMatrix();
        const wallMatrix: number[][] = this.model.getMatrix();
        const figureWidth: number = figureModel.getMatrixWidth();
        const figureHeight: number = figureModel.getMatrixHeight();
        for (let i: number = 0; i < figureHeight; i++) {
            for (let j: number = 0; j < figureWidth; j++) {
                const figureItem: number = figureMatrix[i][j];
                if (figureMatrix[i][j] !== 0) {
                    wallMatrix[y + i][x + j] = figureItem;
                }
            }
        }
        this.model.fillBlocksModelsMap();

        const blocksRowsToExplode: BlockModel[][] = this.getBlocksToExplode();
        if (blocksRowsToExplode) {
            this.explodeBlocks(blocksRowsToExplode);
        }
        this.buildFigure();
    }

    public isFigureCollidesWall(figure: IFigure, x: number, y: number): boolean {
        const figureModel: FigureModel = figure.getModel();
        const figureMatrix: number[][] = figureModel.getMatrix();
        const wallMatrix: number[][] = this.model.getMatrix();
        const figureWidth: number = figureModel.getMatrixWidth();
        const figureHeight: number = figureModel.getMatrixHeight();
        for (let i: number = 0; i < figureHeight; i++) {
            for (let j: number = 0; j < figureWidth; j++) {
                const figureItem: number = figureMatrix[i][j];
                if (figureItem === 1) {
                    if (wallMatrix[y + i][x + j] === 1) {
                        return true;
                    }
                }
            }
        }
    }

    private explodeBlocks(blocksRows: BlockModel[][]): void {
        let explodedRowsNum: number = 0;
        for (let i: number = 0; i < blocksRows.length; i++) {
            const row: BlockModel[] = blocksRows[i];
            const block: BlockModel = row[0];
            const rowPosition: number = block.getOrderY() + explodedRowsNum;
            this.model.getMatrix().splice(rowPosition, 1);
            const emptyRow: number[] = new Array(row.length + 1).join("0").split("").map(parseFloat);
            this.model.getMatrix().unshift(emptyRow);
            explodedRowsNum++;
        }
        this.model.fillBlocksModelsMap();
        this.emit(GameEvent.BLOCKS_EXPLODED, explodedRowsNum);
    }

    public clearAllBlocks(): void {
        (<WallModel>this.model).clear();
        this.model.fillBlocksModelsMap();
        this.buildFigure();
    }

    private getBlocksToExplode(): BlockModel[][] {
        let i: number;
        let j: number;
        let returnVal: BlockModel[][];
        let rowItem: number = undefined;
        const wallMatrix: number[][] = this.model.getMatrix();
        const heightBlocksNumber: number = this.model.getMatrixHeight();
        for (i = heightBlocksNumber - 1; i >= 0; i--) {
            const row: number[] = wallMatrix[i];
            for (j = 0; j < row.length; j++) {
                const item: number = row[j];
                if (j === 0) {
                    rowItem = item;
                } else {
                    if (item !== rowItem) {
                        break;
                    }
                }
            }

            if (j === row.length) {
                if (rowItem === 1) {
                    if (!returnVal) {
                        returnVal = [];
                    }
                    const blocksRow: BlockModel[] = [];
                    for (let k: number = 0; k < row.length; k++) {
                        blocksRow.push(this.model.getBlockModelAt(k, i));
                    }

                    returnVal.push(blocksRow);
                } else {
                    break;
                }
            }
        }

        return returnVal;
    }

    public addListener(event: string | symbol, fn: Function, context?: any): this {
        return this.eventEmitter.addListener(event, fn, context) as this;
    }

    public emit(event: string | symbol, ...args: any[]): boolean {
        return this.eventEmitter.emit(event, args);
    }

    public eventNames(): Array<string | symbol> {
        return this.eventEmitter.eventNames();
    }

    public listenerCount(event: string | symbol): number {
        return this.eventEmitter.listenerCount(event);
    }

    public listeners(event: string | symbol): Array<Function> {
        return this.eventEmitter.listeners(event);
    }

    public off(event: string | symbol, fn?: Function, context?: any, once?: boolean): this {
        return this.eventEmitter.off(event, fn, context, once) as this;
    }

    public on(event: string | symbol, fn: Function, context?: any): this {
        return this.eventEmitter.on(event, fn, context) as this;
    }

    public once(event: string | symbol, fn: Function, context?: any): this {
        return this.eventEmitter.once(event, fn, context) as this;
    }

    public removeAllListeners(event?: string | symbol): this {
        return this.eventEmitter.removeAllListeners(event) as this;
    }

    public removeListener(event: string | symbol, fn?: Function, context?: any, once?: boolean): this {
        return this.eventEmitter.removeListener(event, fn, context, once) as this;
    }

}
