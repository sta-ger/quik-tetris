import * as PIXI from "pixi.js";

import {KeyboardEventType} from "./KeyboardEventType";
import {ResourcesManager} from "./resources/ResourcesManager";
import {ResourcesManagerEvent} from "./resources/ResourcesManagerEvent";
import {GameController} from "./game/GameController";
import {IResourcesManager} from "./resources/IResourcesManager";
import EventEmitter = PIXI.utils.EventEmitter;

export class App extends EventEmitter {
    public static readonly WIDTH_BLOCKS_NUMBER: number = 3;
    public static readonly HEIGHT_BLOCKS_NUMBER: number = 12;
    public static readonly BLOCK_WIDTH: number = 42;
    public static readonly BLOCK_HEIGHT: number = 42;
    public static readonly GAME_PADDING_TOP: number = 25;
    public static readonly GAME_WIDTH: number = App.WIDTH_BLOCKS_NUMBER * App.BLOCK_WIDTH;
    public static readonly GAME_HEIGHT: number = App.HEIGHT_BLOCKS_NUMBER * App.BLOCK_HEIGHT + App.GAME_PADDING_TOP;

    public static resourcesManager: IResourcesManager;

    private gameController: GameController;
    private stage: PIXI.Container;

    constructor() {
        super();
        App.resourcesManager = this.createResourcesManager();
        window.onload = (): void => {
            this.initialize();
        };
    }

    protected initialize(): void {
        this.addKeyboardListeners();
        this.loadResources();
    }

    private loadResources(): void {
        App.resourcesManager.once(ResourcesManagerEvent.RESOURCES_LOADED, () => this.onResourcesLoaded());
        App.resourcesManager.loadResources();
    }

    private onResourcesLoaded(): void {
        const app: PIXI.Application = new PIXI.Application(
            {
                width: this.getGameWidth(),
                height: this.getGameHeight(),
                backgroundColor: 0x000000
            }
        );
        this.stage = app.stage;
        document.body.appendChild(app.view);
        this.gameController = this.createGameController();
    }

    private addKeyboardListeners(): void {
        window.addEventListener(KeyboardEventType.KEY_UP, (e: KeyboardEvent) => this.onKeyUp(e));
        window.addEventListener(KeyboardEventType.KEY_DOWN, (e: KeyboardEvent) => this.onKeyDown(e));
    }

    private onKeyUp(e: KeyboardEvent): void {
        this.emit(KeyboardEventType.KEY_UP, e.key);
    }

    private onKeyDown(e: KeyboardEvent): void {
        this.emit(KeyboardEventType.KEY_DOWN, e.key);
    }

    protected createResourcesManager(): ResourcesManager {
        return new ResourcesManager();
    }

    protected createGameController(): GameController {
        return new GameController(this);
    }

    public getStage(): PIXI.Container {
        return this.stage;
    }

    public getGameWidth(): number {
        return App.GAME_WIDTH;
    }

    public getGameHeight(): number {
        return App.GAME_HEIGHT;
    }

    public getGamePaddingTop(): number {
        return App.GAME_PADDING_TOP;
    }

}
