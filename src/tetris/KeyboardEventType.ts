export class KeyboardEventType {

    public static readonly KEY_UP: string = "keyup";
    public static readonly KEY_DOWN: string = "keydown"

}
