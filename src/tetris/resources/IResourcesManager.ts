import * as PIXI from "pixi.js";
import EventEmitter = PIXI.utils.EventEmitter;

export interface IResourcesManager extends EventEmitter {

    loadResources(): void;

    createSprite(resourceId: string): PIXI.Sprite;

    getJson(resourceId: string): {};

}