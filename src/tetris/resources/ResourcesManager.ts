import * as PIXI from "pixi.js";
import {Resource} from "./Resource";
import {ResourcesManagerEvent} from "./ResourcesManagerEvent";
import {IResourcesManager} from "./IResourcesManager";
import EventEmitter = PIXI.utils.EventEmitter;

export class ResourcesManager extends EventEmitter implements IResourcesManager {
    private readonly resourcesPath: string;
    private readonly texturesToLoad: string[];
    private readonly jsonsToLoad: string[];
    private readonly loader: PIXI.Loader;
    private resources: {};

    constructor() {
        super();
        this.resourcesPath = "./assets/";
        this.texturesToLoad = [
            Resource.TEXTURE_BLOCK_GREY,
            Resource.TEXTURE_BLOCK_YELLOW,
            Resource.TEXTURE_BLOCK_VIOLET,
            Resource.TEXTURE_BLOCK_PINK,
            Resource.TEXTURE_BLOCK_GREEN,
            Resource.TEXTURE_BLOCK_BLUE,
        ];
        this.jsonsToLoad = [
            Resource.JSON_FIGURES
        ];
        this.loader = new PIXI.Loader();
    }

    public loadResources(): void {
        this.texturesToLoad.forEach((resource: string) => {
            this.loader.add(resource, this.resourcesPath + resource + ".png");
        });
        this.loader.load((loader: PIXI.Loader, resources: {}) => this.onTexturesLoaded(resources));
    }

    private async onTexturesLoaded(resources: {}): Promise<void> {
        this.fillTextures(resources);
        await this.loadJsons();
        this.emit(ResourcesManagerEvent.RESOURCES_LOADED);
    }

    private async loadJsons(): Promise<void> {
        for (let i: number = 0; i < this.jsonsToLoad.length; i++) {
            const resourceId: string = this.jsonsToLoad[i];
            const response: Response = await fetch(this.resourcesPath + resourceId + ".json");
            this.resources[resourceId] = await response.text();
        }
    }

    private fillTextures(loadedResources: {}): void {
        let resource: any;
        this.resources = {};
        for (const resourceName in loadedResources) {
            if (loadedResources.hasOwnProperty(resourceName)) {
                resource = loadedResources[resourceName];
                this.resources[resourceName] = resource.texture;
            }
        }
    }

    public createSprite(resourceId: string): PIXI.Sprite {
        const texture: PIXI.Texture = this.getTexture(resourceId);
        return new PIXI.Sprite(texture);
    }

    private getTexture(resourceId: string): PIXI.Texture {
        return this.resources[resourceId];
    }

    public getJson(resourceId: string): {} {
        return JSON.parse(this.resources[resourceId]);
    }

}
