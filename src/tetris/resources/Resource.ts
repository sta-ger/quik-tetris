export class Resource {

	public static readonly JSON_FIGURES: string = "figures";

	public static readonly TEXTURE_BLOCK_GREY: string = "block_grey";
	public static readonly TEXTURE_BLOCK_YELLOW: string = "block_yellow";
	public static readonly TEXTURE_BLOCK_VIOLET: string = "block_violet";
	public static readonly TEXTURE_BLOCK_PINK: string = "block_pink";
	public static readonly TEXTURE_BLOCK_GREEN: string = "block_green";
	public static readonly TEXTURE_BLOCK_BLUE: string = "block_blue";

}
