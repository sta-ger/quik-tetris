export class KeyboardEventKey {

    public static readonly LEFT: string = "ArrowLeft";
    public static readonly RIGHT: string = "ArrowRight"
    public static readonly UP: string = "ArrowUp"
    public static readonly DOWN: string = "ArrowDown"

}
