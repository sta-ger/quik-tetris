export type SingleFallingFigureJsonData = {
    figure: string,
    initialState: string
}

export type SingleFigureJsonData = {
    color: string,
    states: number[][][],
}

export type FiguresJsonData = {
    availableFigures: SingleFigureJsonData[],
    fallingFigures: SingleFallingFigureJsonData[],
}